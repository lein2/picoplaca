PicoPlaca
Author: Daniel Ochoa
=================================================================
Configuration Requirements
=================================================================
- Node: minimun V10.9
- Angular CLI v8.0.3


=================================================================
Configuration Instructions
=================================================================
- Download the directory with the project
- Inside the directory run the command: 
	#npm install
- After all the dependencies have been Installed run the following command to run angular server in localhost:4200
	#ng serve
- In web browser enter: http://localhost:4200/
