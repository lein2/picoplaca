import { Component, OnInit } from '@angular/core';

/**
 * Author: Daniel Ochoa
 * Component pico-placa
 * This Component takes a Number Plate of a car, a Date and an Hour
 * and predicts if the car can or not be on the road at that time of that
 * day by following "Pico y Paca" law.
 */

@Component({
  selector: 'app-pico-placa',
  templateUrl: './pico-placa.component.html',
  styleUrls: ['./pico-placa.component.css']
})
export class PicoPlacaComponent implements OnInit {

  /**
   * Variables used to store user entries
   */
  inputPlate: string;
  inputDate: string;
  inputHour: string;

  /**
   * Constant Variable days of the week.
   */
  weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];


  /**
   * Constant Variables of hours of the day that applies "Pico y Paca" law.
   */
  picoPlacaMorningStart = 7 * 60;
  picoPlacaMorningEnd = 9 * 60 + 30;

  picoPlacaAfternoonStart = 16 * 60;
  picoPlacaAfternoonEnd = 19 * 60 + 30;

  constructor() { }

  ngOnInit() {

  }

  /**
   * Function that checks if the number plate entered by the user
   * Follows the correct format of Ecuador number plates: ABC-0123
   */
  chackPlateNumber(plate: string){
    const regexNormalPlate = new RegExp('[A-Z][A-Z][A-Z]-\\d{4}');
    var chkPlate = false;
    if(regexNormalPlate.test(plate)){
      chkPlate = true;
    } else {
      chkPlate = false;
    }
    return chkPlate;

  }

  /**
   * Function that checks if the date entered by the user
   * Follows the correct ISO JS format: YYYY-MM-DD
   */
  chkDate(date: string){
    const regexDate = new RegExp('\\d{4}-([0][1-9]|[1][0-2])-([0][1-9]|[1-2]\\d|[3][0-1])');
    var chkDt = false;
    if(regexDate.test(date)){
      chkDt = true;
    } else {
      chkDt = false;
    }

    return chkDt;
  }

  /**
   * Uses the functions above to either send an alert to correct the imputs
   * or beginning the function that does the prediction if all the imputs are correct.
   */
  getData(){
    this.inputPlate = (<HTMLInputElement>document.getElementById('txtPlate')).value.toLocaleUpperCase();
    this.inputDate =  (<HTMLInputElement>document.getElementById('txtDate')).value;
    this.inputHour =  (<HTMLInputElement>document.getElementById('dateTime')).value;

    var msg ='';
    var correct = true;

    if(!this.chkDate(this.inputDate)){
      msg += 'Incorrect Date Input. The format of the date is YYYY-MM-DD';
      correct = false;
    }

    if (!this.chackPlateNumber(this.inputPlate)){
      msg += '\nIncorrect Plate Number Input. The format of the plate is ABC-1234';
      correct = false;
    }

    if(this.inputHour === ''){
      msg += '\nIncorrect Time Input. Please Select an Hour';
      correct = false;
    }

    if(!correct){
      alert(msg);
    } else {
      this.picoPlaca(this.inputPlate, this.inputDate, this.inputHour);
    }
  }

  /**
   * Function that checks if the time entered by the user is between the
   * hours stipulated by the law of "Peak and Plate".
   */
  chkHourPicoPlaca(hour: number){
    var chkhour = false;
    if(hour >= this.picoPlacaMorningStart && hour < this.picoPlacaMorningEnd){
      chkhour = true; 
    }

    if(hour >= this.picoPlacaAfternoonStart && hour < this.picoPlacaAfternoonEnd){
      chkhour = true; 
    }

    return chkhour;
  }

  /**
   * Primary function that makes the prediction using user's imputs.
   */
  picoPlaca(plate: string, date: string, hour: string){
    // Get the last digit of the number plate
    var plateLastDigit = plate.charAt(plate.length -1);
    var dateParts = date.split('-');
    var timeParts = hour.split(':');
    var chkdayPicoPlaca = false;

    // Get minutes of hte day using the time that the user entered.
    var minutesDay = (parseInt(timeParts[0])*60) + parseInt(timeParts[1]);

    //Get the weekday of the date entered by the user
    var dateIso = dateParts[0] + '-' + parseInt(dateParts[1]).toString() + '-' + parseInt(dateParts[2]).toString();

    var dateJS = new Date(dateIso);

    var wkday = this.weekday[dateJS.getDay()];

    //Predictor
    switch(wkday){
      case 'Sunday':
        (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#CBE5FE";
        (<HTMLInputElement>document.getElementById('result')).textContent = 'Weekends do not apply Pico y Placa.';
        (<HTMLInputElement>document.getElementById('result1')).textContent = '';
        break;

      case 'Monday':
        if(plateLastDigit === '1' || plateLastDigit === '2'){
          chkdayPicoPlaca = true;
        }
        if(chkdayPicoPlaca && this.chkHourPicoPlaca(minutesDay)){
          (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#F7D7DA";
          (<HTMLInputElement>document.getElementById('result')).textContent = 'Car can NOT be on the road';
          (<HTMLInputElement>document.getElementById('result1')).textContent = 'it\'s Pico y Placa time.';
        } else {
          (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#D6EBD8";
          (<HTMLInputElement>document.getElementById('result')).textContent = 'Car CAN be on the road';
          (<HTMLInputElement>document.getElementById('result1')).textContent = 'Pico y Placa time it\'s over';
        }
        break;

        case 'Tuesday':
        if(plateLastDigit === '3' || plateLastDigit === '4'){
          chkdayPicoPlaca = true;
        }
        if(chkdayPicoPlaca && this.chkHourPicoPlaca(minutesDay)){
          (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#F7D7DA";
          (<HTMLInputElement>document.getElementById('result')).textContent = 'Car can NOT be on the road';
          (<HTMLInputElement>document.getElementById('result1')).textContent = 'it\'s Pico y Placa time.';
        } else {
          (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#D6EBD8";
          (<HTMLInputElement>document.getElementById('result')).textContent = 'Car CAN be on the road';
          (<HTMLInputElement>document.getElementById('result1')).textContent = 'Pico y Placa time it\'s over';
        }
        break;

        case 'Wednesday':
        if(plateLastDigit === '5' || plateLastDigit === '6'){
          chkdayPicoPlaca = true;
        }
        if(chkdayPicoPlaca && this.chkHourPicoPlaca(minutesDay)){
          (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#F7D7DA";
          (<HTMLInputElement>document.getElementById('result')).textContent = 'Car can NOT be on the road';
          (<HTMLInputElement>document.getElementById('result1')).textContent = 'it\'s Pico y Placa time.';
        } else {
          (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#D6EBD8";
          (<HTMLInputElement>document.getElementById('result')).textContent = 'Car CAN be on the road';
          (<HTMLInputElement>document.getElementById('result1')).textContent = 'Pico y Placa time it\'s over';
        }
        break;

        case 'Thursday':
          if(plateLastDigit === '7' || plateLastDigit === '8'){
            chkdayPicoPlaca = true;
          }
          if(chkdayPicoPlaca && this.chkHourPicoPlaca(minutesDay)){
            (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#F7D7DA";
            (<HTMLInputElement>document.getElementById('result')).textContent = 'Car can NOT be on the road';
            (<HTMLInputElement>document.getElementById('result1')).textContent = 'it\'s Pico y Placa time.';
          } else {
            (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#D6EBD8";
            (<HTMLInputElement>document.getElementById('result')).textContent = 'Car CAN be on the road';
            (<HTMLInputElement>document.getElementById('result1')).textContent = 'Pico y Placa time it\'s over';
          }
          break;

        case 'Friday':
          if(plateLastDigit === '9' || plateLastDigit === '0'){
            chkdayPicoPlaca = true;
          }
          if(chkdayPicoPlaca && this.chkHourPicoPlaca(minutesDay)){
            (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#F7D7DA";
            (<HTMLInputElement>document.getElementById('result')).textContent = 'Car can NOT be on the road';
            (<HTMLInputElement>document.getElementById('result1')).textContent = 'it\'s Pico y Placa time.';
          } else {
            (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#D6EBD8";
            (<HTMLInputElement>document.getElementById('result')).textContent = 'Car CAN be on the road';
            (<HTMLInputElement>document.getElementById('result1')).textContent = 'Pico y Placa time it\'s over';
          }
          break;

        case 'Saturday':
            (<HTMLInputElement>document.getElementById('resultContainer')).style.background = "#CBE5FE";
          (<HTMLInputElement>document.getElementById('result')).textContent = 'Weekends do not apply Pico y Placa.';
          (<HTMLInputElement>document.getElementById('result1')).textContent = '';
          break;
    }

  }





}
